-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Sty 2022, 08:25
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `id` int(11) NOT NULL,
  `tresc pytania` text COLLATE utf8_polish_ci NOT NULL,
  `a` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `b` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `c` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `d` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `prawidlowa odpowiedz` varchar(250) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id`, `tresc pytania`, `a`, `b`, `c`, `d`, `prawidlowa odpowiedz`) VALUES
(1, 'Jak nazywa si? metoda sortowania polegaj?ca na podziale na n przedzia??w jednakowej d?ugo?ci, w kt?rych\r\nnast?puje sortowanie, po czym posortowane zawarto?ci przedzia??w s? poddawane analizie i prezentacji?\r\n', 'Sortowanie szybkie', 'Sortowanie kube?kowe', 'Sortowanie b?belkowe.', 'Sortowanie przez wyb?r.', 'B'),
(2, 'Program zapisany w j?zyku PHP ma za zadanie obliczy? ?redni? pozytywnych ocen ucznia od 2 do 6.\r\nWarunek wybierania ocen w p?tli licz?cej ?redni? powinien zawiera? wyra?enie logiczne', '$ocena > 2 or $ocena < 6', '$ocena > 2 and $ocena < 6', '$ocena >= 2 or $ocena <= 6', '$ocena >= 2 and $ocena <= 6', 'D'),
(3, 'W j?zyku C++ zdefiniowano zmienn?: char zm1;. W jaki spos?b mo?na do niej przypisa? warto?? zgodnie\r\nze sk?adni? j?zyka?', 'zm1 = \'w\';', 'zm1 == 0x35;\r\n', 'zm1[2] = 32;', 'zm1 = \"wiadro\";', 'A'),
(4, 'W j?zyku JavaScript, aby wydzieli? fragment napisu znajduj?cy si? pomi?dzy wskazanymi przez parametr\r\nindeksami nale?y u?y? metody', 'trim()', 'slice()', 'concat()', 'replace()', 'B'),
(5, 'W jaki spos?b, stosuj?c j?zyk PHP, zapisa? w ciasteczku napis znajduj?cy si? w zmiennej dane na czas\r\njednego dnia?', 'setcookie(\"dane\", $dane, 0);', 'setcookie(\"dane\", \"dane\", 0);', ' setcookie(\"dane\", $dane, time());', 'setcookie(\"dane\", $dane, time() + (3600*24));', 'D'),
(6, 'Kt?ry zapis definiuje w j?zyku PHP komentarz wieloliniowy?', '#', '//', '/* */', '<!-- -->', 'C'),
(7, 'Kt?ry z typ?w relacji wymaga utworzenia tabeli po?redniej ??cz?cej klucze g??wne obu tabel?\r\n', '1..1', '1..n', 'n..1', 'n..m', 'D'),
(8, 'Integralno?? encji w bazie danych zostanie zachowana, je?eli mi?dzy innymi', 'klucz g??wny b?dzie zawsze liczb? ca?kowit?', 'ka?dej kolumnie zostanie przypisany typ danych', 'dla ka?dej tabeli zostanie utworzony klucz g??wny.', 'ka?dy klucz g??wny b?dzie mia? odpowiadaj?cy mu klucz obcy w innej tabeli.', 'C'),
(9, 'Aby przy pomocy zapytania SQL zmodyfikowa? struktur? istniej?cej tabeli, nale?y zastosowa? kwerend?', 'UPDATE', 'INSERT INTO', 'ALTER TABLE', 'CREATE TABLE', 'C'),
(10, 'Kt?rego typu danych w bazie MySQL nale?y u?y?, aby przechowa? w jednym polu dat? i czas?', 'DATE', 'YEAR', 'BOOLEAN', 'TIMESTAMP\r\n', 'D');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
